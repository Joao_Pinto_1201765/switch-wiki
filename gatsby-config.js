module.exports = {
  siteMetadata: {
    siteTitle: `Switch 2020-21 Wiki`,
    defaultTitle: `Switch 2020-21 Wiki`,
    siteTitleShort: `Switch Wiki`,
    siteDescription: `Local de partilha de informação dos alunos do Switch 2020-21, parceria ISEP -- Porto Tech Hub`,
    siteUrl: `https://switch20-wiki.netlify.app`,
    siteAuthor: `@rocketseat`,
    siteImage: `/banner.png`,
    siteLanguage: `pt`,
    themeColor: `#FF0000`,
    basePath: `/`,
  },
  plugins: [
    {
      resolve: `@rocketseat/gatsby-theme-docs`,
      options: {
        configPath: `src/config`,
        docsPath: `src/docs`,
        githubUrl: `https://bitbucket.org/nunocasteleira_switch/switch-wiki/`,
        baseDir: `examples/gatsby-theme-docs`,
      },
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Rocketseat Gatsby Themes`,
        short_name: `RS Gatsby Themes`,
        start_url: `/`,
        background_color: `#ffffff`,
        display: `standalone`,
        icon: `static/favicon.png`,
      },
    },
    `gatsby-plugin-sitemap`,
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        // trackingId: ``,
      },
    },
    `gatsby-plugin-remove-trailing-slashes`,
    {
      resolve: `gatsby-plugin-canonical-urls`,
      options: {
        siteUrl: `https://switch20-wiki.netlify.app`,
      },
    },
    `gatsby-plugin-offline`,
  ],
};
