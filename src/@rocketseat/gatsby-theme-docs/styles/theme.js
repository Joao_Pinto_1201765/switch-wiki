export default {
  colors: {
    primary: "#931a25",
    background: "#FFFFFF",
    shape: `#f5d5d5`,
    title: `#f20d28`,
    text: `#1F1715`,
  },
};
