## Objetivo

Este repositório contém os _dev files_ do site [Switch 2020-21 Wiki](https://switch20-wiki.netlify.app/).
Idealmente, os textos nele contidos surgirão da partilha de grande parte dos alunos, para que o conhecimento não seja
um segredo para ninguém.

Para poderes contribuir tens as instruções na Wiki, mas em suma:

1. Fork it
1. Clone it to your local system
1. Make a new branch
1. Make your changes
1. Push it back to your repo
1. Issue a pull request button
1. If the reviewers ask for changes, repeat steps 5 and 6 to add more commits to your pull request.

## ⚡️ Getting started

1. Install node.js

2. Install Yarn

   ```bash
   npm install -g yarn
   ```

3. Install Gatsby.

   ```bash
   npm install -g gatsby-cli
   ```

4. Start developing.

   ```bash
   yarn    # create dependencies
   gatsby develop
   ```

5. Are you ready for launch?

   Your site is now running at `http://localhost:8000`

---

Made with 💜 by Rocketseat :wave: [check our community!](https://discordapp.com/invite/gCRAFhc)
